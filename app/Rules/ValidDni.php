<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidDni implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return validate_dni($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'dni no Valido';
    }
}
