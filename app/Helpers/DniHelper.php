<?php

namespace App\Helpers;

class DniHelper
{
  public static function validate(string $dni)
  {
    return (bool) preg_match('/^[0-9]{8}[A-Za-z]$/', $dni);
  }
}