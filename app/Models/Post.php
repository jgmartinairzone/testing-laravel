<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = Str::lower($value);
    }

    public function getSlugAttribute()
    {
        return Str::slug($this->name);
    }

    public function getHrefAttribute()
    {
        return route('post', ['slug' => $this->slug]);
    }
}
