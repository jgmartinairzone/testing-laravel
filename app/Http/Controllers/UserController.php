<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostUserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;

class UserController extends Controller
{
    public function __invoke(PostUserRequest $request)
    {
        $new_user = User::create($request->all());
        $user = new UserResource($new_user);
        $body = compact('user');
        return response()->json(compact('body'), 201);
    }
}
