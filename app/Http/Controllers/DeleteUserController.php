<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeleteUserRequest;
use App\Models\User;
use App\Rules\ValidDni;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class DeleteUserController extends Controller
{
    public function __invoke(DeleteUserRequest $request)
    {
        $deleted = User::where('dni', $request->dni)->delete();
        return response()->json($deleted, 200);
    }
}
