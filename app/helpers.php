<?php

if (!function_exists('validate_dni')) {
  function validate_dni(string $dni)
  {
    return App\Helpers\DniHelper::validate($dni);
  }
}