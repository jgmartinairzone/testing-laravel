<?php

namespace Tests\Feature\app\Models;

use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function test_name_is_required_fails()
    {
        $user_data = User::factory(1)->make()->first()->toArray();
        $user_data['name'] = '';

        $response = $this->postJson('/api/users', $user_data);

        $response->assertStatus(422);
    }

    public function test_dni_is_required_fails()
    {
        $user_data = User::factory(1)->make()->first()->toArray();
        $user_data['dni'] = 'no soy un dni';

        $response = $this->postJson('/api/users', $user_data);

        $response->assertStatus(422);
    }
    
    public function test_creation_ok()
    {
        $user_data = User::factory(1)->make()->first()->makeVisible('password');
        $user_spect = new UserResource($user_data);
        $response = $this->postJson('/api/users', $user_data->toArray());

        $response
        ->assertCreated()
        ->assertExactJson([
            'body' => [
                'user' => $user_spect->toArray(request())
            ]
        ]);
    }

    public function test_delete_fails_when_dni_is_not_valid()
    {
        $dni = 'no soy un dni';
        User::factory()->create([
            'dni' => $dni
        ]);
        
        $response = $this->deleteJson('/api/users', ['dni' => $dni]);

        $response->assertStatus(422);
    }

    public function test_delete_user_by_dni_ok()
    {
        $dni = '12345678Z';
        User::factory()->create([
            'dni' => $dni
        ]);
        
        $response = $this->delete('/api/users', ['dni' => $dni]);

        $response->assertStatus(200);
        $this->assertDatabaseMissing('users', [
            'dni' => $dni
        ]);
    }
}
