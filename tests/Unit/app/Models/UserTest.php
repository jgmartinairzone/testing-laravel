<?php

namespace Tests\Unit\app\Models;

use App\Models\User;
use App\Models\UserContact;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function test_relation_user_contact_ok()
    {
        $user = User::factory()->create();
        $contact = UserContact::factory()->create([
            'user_id' => $user->id,
        ]);

        $this->assertInstanceOf(UserContact::class, $user->user_contact);
    }
}
