<?php

namespace Tests\Unit\app\Models;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PostTest extends TestCase
{
    use RefreshDatabase;

    // Crear y que se cree el nombre en minúsculas
    public function test_name_to_lowercase()
    {
        // Arrange
        $name = 'Post de Prueba';
        $name_expected = 'post de prueba';

        // Act
        $post = Post::create(['name' => $name]);

        // Asserts
        $this->assertSame($post->name, $name_expected);
    }
    
    // Crear y que se configure el slug
    public function test_slug_from_name()
    {
        // Arrange
        $name = 'Post de Prueba';
        $slug_expected = 'post-de-prueba';

        // Act
        $post = Post::create(['name' => $name]);

        // Asserts
        $this->assertSame($post->slug, $slug_expected);
    }


    // Obtener ruta
    public function test_get_href_from_accessor()
    {
        // Arrange
        $name = 'Post de Prueba';
        $href_expected = route('post', ['slug' => 'post-de-prueba']);

        // Act
        $post = Post::create(['name' => $name]);

        // Asserts
        $this->assertSame($post->href, $href_expected);   
    }
    
}
