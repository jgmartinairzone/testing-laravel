<?php

namespace Tests\Unit\app\Helpers;

use App\Helpers\DniHelper;
use PHPUnit\Framework\TestCase;

class DniHelperTest extends TestCase
{
    public function test_dni_is_valid()
    {
        $dni = '12345678Z';

        $result = DniHelper::validate($dni);

        $this->assertTrue($result);
    }

    public function test_dni_is_invalid()
    {
        $dni = 'no soy un dni';

        $result = DniHelper::validate($dni);

        $this->assertFalse($result);
    }
}
